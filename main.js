class WhoIsTalking {
	setEvents(message, html, data) {
		$(html).hover(this._onHover.bind(this), this._onHoverOut.bind(this));
	}

	_onHover(event) {
		event.preventDefault();
	    if ( !canvas.scene.data.active ) return;
	    const message = game.messages.get(event.currentTarget.dataset.messageId)
	    const token = canvas.tokens.get(message.data.speaker.token);
	    if ( token && token.isVisible ) {
	      if ( !token._controlled ) token._onMouseOver(event);
	      this._highlighted = token;
	    }
	}

	_onHoverOut(event) {
		event.preventDefault();
		if ( !canvas.scene.data.active ) return;
		if ( this._highlighted ) this._highlighted._onMouseOut(event);
		this._highlighted = null;		
	}
}

WhoIsTalkingSingleton = new WhoIsTalking();
Hooks.on('renderChatMessage', WhoIsTalkingSingleton.setEvents.bind(WhoIsTalkingSingleton));